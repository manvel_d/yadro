<?php
require_once(__DIR__ . '/autoload.php');

const STATUS_SUCCESS = 142;

// Возвращает массив клиентов
function getClients() {
    return [
        [
            'id' => 2,
            'name' => 'artedegrass0',
            'api' => '23bc075b710da43f0ffb50ff9e889aed',
        ],
    ];
}

// Возвращает сделки клиента
function getClientLeads($api, $crm_user_id, $status, $id, $ifmodif, $count, $offset){
    $fullResult = [];// Полный список сделок

    try {
        $resultCount = $count;

        // Реализована пагинация. Проверка, что вернулось сделки не закончились. Если вернулось меньше ожидаемого - цикл прервётся
        while($resultCount == $count) {
            // Выполняем запрос на получение сделок
            $result = $api->lead->getAll($crm_user_id, $status, $id, $ifmodif, $count, $offset)['result'];
            // Добавляем полученные сделки к общему списку
            $fullResult = array_merge($fullResult, $result);
            // Увеличиваем смещение
            $offset += $count;
            // Сохраняем кол-во полученных сделок для проверки в условии цикла
            $count = count($result);
        };

        return $fullResult;
    } catch (Exception $e) {
        echo 'Exception when calling LeadApi->getAll: ', $e->getMessage(), PHP_EOL;
        return null;
    }
}


// Возвращает сумму сделок клиента
function getClientBudgetSum($api, $startDate, $endDate) {
    $crm_user_id = null; //int[] | фильтр по id ответственного
    $status = [STATUS_SUCCESS]; // int[] | фильтр по id статуса
    $id = null; // int[] | фильтр по id
    $ifmodif = null; // string | фильтр по дате изменения. timestamp или строка в формате 'D, j M Y H:i:s'
    $count = 500; // int | Количество запрашиваемых элементов
    $offset = 0; // int | смещение, относительно которого нужно вернуть элементы

    $leads = getClientLeads($api, $crm_user_id, $status, $id, $ifmodif, $count, $offset); //получаем список сделок

    $sum = 0;// Сумма бюджета

    // Проходим по списку сделок
    foreach ($leads as $lead) {
        // Если сделка закрыта в указанный период, добавляем её сумму к бюджету
        if($lead['date_close'] >= $startDate && $lead['date_close'] <= $endDate) {
            $sum += intval($lead['price']);
        }
    }

    return $sum;
}

$startDate = strtotime(htmlspecialchars($_GET['startDate']));// Получаем начало периода
$endDate = strtotime(htmlspecialchars($_GET['endDate']));// Получаем конец периода

$fullSum = 0;// Общая сумма бюджетов

// Проверяем, что переданы даты начала и конца периода
if($startDate && $endDate) {
    $clients = getClients();// Получаем клиентов

    $budgets = [];// Массив строк для вывода

    foreach ($clients as $client) {
        Introvert\Configuration::getDefaultConfiguration()->setApiKey('key', $client['api']); // Прописываем ключ
        $api = new Introvert\ApiClient();// Создаём экземпляр клиента

        $clientBudgetSum = getClientBudgetSum($api, $startDate, $endDate);// Получаем сумму бюджета клиента

        // Добавляем запись в массив бюджетов для вывода
        $budget = ['id' => $client['id'], 'name' => $client['name'], 'sum' => $clientBudgetSum];
        $budgets[] = $budget;

        $fullSum += $clientBudgetSum;// Добавляем к общей сумме
    };

    // Выводим таблицу
    echo '<h1>Бюджеты</h1>';
    echo '<table border="1">';
    echo '<td>ID клиента</td>';
    echo '<td>Имя</td>';
    echo '<td>Сумма бюджета</td>';
    echo '</tr>';
    echo '<tr>';
    foreach ($budgets as $budget) {
        echo '<tr>';
        echo '<td>' . $budget['id'] . '</td>';
        echo '<td>' . $budget['name'] . '</td>';
        echo '<td>' . $budget['sum'] . '</td>';
        echo '</tr>';
    }
    echo '</table>';

    echo '<p>Общая сумма = ' . $fullSum . '</p>';
} else {
    echo 'Не задан период';
}