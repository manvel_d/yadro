<?php
header('Content-type: application/json');
require_once(__DIR__ . '/autoload.php');

use components\Configuration;
use models\Lead;

try {
    $conf = new Configuration( __DIR__ . '/src/config/config.php');// создаём экземпляр конфигурации

    Introvert\Configuration::getDefaultConfiguration()->setApiKey('key', $conf->getClientParam('api')); // Прописываем ключ
    $api = new Introvert\ApiClient();// Создаём экземпляр клиента

    // получаем дату начала и конца периода
    $startDate = strtotime(date('d.m.Y')); // текущий день
    if (!$startDate)
        throw new Exception('Неправильная начальная дата');
    $endDate = $startDate + 30 * 24 * 60 * 60; // дата конца = дата начала + 30 дней

    // получаем параметры поиска из конфигуратора
    $crm_user_id = $conf->getRequestParam('crm_user_id');
    $id = $conf->getRequestParam('id');
    $ifmodif = $conf->getRequestParam('ifmodif');
    $count = $conf->getRequestParam('count');
    $offset = $conf->getRequestParam('offset');
    $field_id = $conf->getRequestParam('field_id');
    $status = $conf->getRequestParam('status');;

    $leads = Lead::getAll($api, $crm_user_id, $status, $id, $ifmodif, $count, $offset);// получаем список сделок

    $leadsCalendar = [];// Календарь сделок - ассоциативный массив 'дата' => 'кол-во сделок'

    $date = date('d.m.Y');
    // Инициализируем календарь
    for($i = 0; $i <= 29; $i++) {
        $date = date('d.m.Y',strtotime("+" . $i ." day"));
        $leadsCalendar[$date] = 0;
    }

    // Проходим по списку сделок
    foreach ($leads as $lead) {
        $checkDate = strtotime($lead['custom_fields'][0]['values'][0]['value']);// дата из кастомного поля

        // Проверяем, что сделка в нужном статусе
        if (in_array($lead['status_id'], $status)) {
            // Проходим в цикле по кастомным полям
            foreach ($lead['custom_fields'] as $field) {
                // Если нашли нужный id
                if ($field['id'] == $field_id) {
                    // Получаем дату из поля
                    $checkDate = strtotime($field['values'][0]['value']);

                    // Если дата из кастомного поля попадает в промежуток startDate-endDate
                    if ($startDate <= $checkDate && $endDate > $checkDate) {
                        $day = date('d.m.Y', $lead['date_create']); // получаем день
                        $leadsCalendar[$day] += 1; // увеличиваем кол-во сделок в дне на 1
                    };
                }
            }
        }
    };

    echo json_encode($leadsCalendar); //возвращаем ответ в json
} catch (\Exception $ex) {
    echo json_encode(['error' => $ex->getMessage()]); //возаращаем ошибку в json
}