<?php
namespace components;

/*
 * Содержит настройки клиента и запроса
 */
class Configuration
{
    private $config = [];

    public function __construct($path)
    {
        $this->config = require $path; // Получаем массив настроек из файла
    }

    /*
     * Проверят существование объязательных настроек
     */
    public function check()
    {
        if ($this->config['client']['api'] == null || $this->config['request']['date_field_id'] == null)
            throw new \Exception('Ошибка конфигурации');
    }

    /*
     * Возвращает настройки клиента по названию параметра
     */
    public function getClientParam($param) {
        return $this->config['client'][$param] ?? null;
    }

    /*
     * Возвращает настройки запроса по названию параметра
     */
    public function getRequestParam($param) {
        return $this->config['request'][$param] ?? null;
    }

}