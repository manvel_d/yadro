<?php
/**
 * Created by PhpStorm.
 * User: sMDepelyan
 * Date: 14.05.2019
 * Time: 16:15
 */

namespace models;


class Client
{
    public $id;
    public $email;
    public $api;

    public function __construct($id, $email, $api)
    {
        $this->id = $id;
        $this->email = $email;
        $this->api = $api;
    }
}