<?php
namespace models;

/*
 * Содержит методы для работы со сделками
 */
class Lead
{
    /*
     * выполняет запрос search API, с использованием пагинации
     */
    public static function search($api, $query, $count, $offset)
    {
        try {
            $fullResult = [];// Полный список сделок
            $resultCount = $count; // Кол-во вернувшихся на запрос сделок

            // Реализована пагинация. Проверка, что вернулось сделки не закончились. Если вернулось меньше ожидаемого - цикл прервётся
            while($resultCount == $count) {
                $result = $api->lead->search($query, $count, $offset)['result'];// Выполняем запрос на получение сделок
                $fullResult = array_merge($fullResult, $result);// Добавляем полученные сделки к общему списку
                $offset += $count;// Увеличиваем смещение
                $count = count($result);// Сохраняем кол-во полученных сделок для проверки в условии цикла
            };

            return $fullResult;
        } catch (\Exception $e) {
            throw new \Exception('Ошибка в методе: Lead::search' . $e->getMessage());
        }
    }

    /*
     * Выполняет запрос getAll API, с использованием пагинаации
     */
    public static function getAll($api, $crm_user_id, $status, $id, $ifmodif, $count, $offset)
    {
        try {
            $fullResult = [];// Полный список сделок

            $resultCount = $count;// Кол-во вернувшихся на запрос сделок

            // Реализована пагинация. Проверка, что вернулось сделки не закончились. Если вернулось меньше ожидаемого - цикл прервётся
            while($resultCount == $count) {
                $result = $api->lead->getAll($crm_user_id, $status, $id, $ifmodif, $count, $offset);// Выполняем запрос на получение сделок
                $fullResult = array_merge($fullResult, $result);// Добавляем полученные сделки к общему списку
                $offset += $count;// Увеличиваем смещение
                $count = count($result);// Сохраняем кол-во полученных сделок для проверки в условии цикла
            };

            return $fullResult['result'];
        } catch (\Exception $e) {
            throw new \Exception('Ошибка в методе: Lead::getAll' . $e->getMessage());
        }
    }
}